import React, { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from './screen/Home';
import Detail from './screen/Detail';

const Stack = createNativeStackNavigator();

const ItemsContext = React.createContext([]);

const ItemsProvider = ({ children }) => {
    return (
        <ItemsContext.Provider value={useState([])}>
            {children}
        </ItemsContext.Provider>
    );
}

const App = () => {
    return (
        <ItemsProvider>
            <NavigationContainer >
                <Stack.Navigator initialRouteName="Home"
                    screenOptions={{
                        headerShown: false
                    }}>
                    <Stack.Screen name="Home" component={Home} />
                    <Stack.Screen name="Detail" component={Detail} />
                </Stack.Navigator>
            </NavigationContainer>
        </ItemsProvider>
    );
};

export default App;