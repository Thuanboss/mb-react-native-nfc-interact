import * as React from 'react';
import { StyleSheet, View, Text, Button, Alert } from 'react-native';
import MbReactNativeNfcInteract, { NFC_DISCOVERED, NFC_READ_CARD_SUCCESS } from 'mb-react-native-nfc-interact';
import MbReactNativeMrzJs, { MRZ_DATA, ID_CARD } from 'mb-react-native-mrz';
import AsyncStorage from '@react-native-community/async-storage';
import { get } from 'lodash';

const storeData = async (key: string, value: any) => {
    try {
        const strVal = JSON.stringify(value);
        await AsyncStorage.setItem(key, strVal);
    } catch (error) {
        console.log(error);
    }
};

const loadData = async (key: string) => {
    try {
        const value = await AsyncStorage.getItem(key);
        if (value !== null) {
            return JSON.parse(value);
        }
    } catch (error) {
        console.log(error);
    }
}

const removeItem = async (key) => {
    try {
        AsyncStorage.removeItem(key);

    } catch (error) {
        console.log(error);
    }
}

const showAlert = (message: string) => {
    Alert.alert(
        "Thông báo",
        message,
        [
            {
                text: "Đóng",
                onPress: () => { },
                style: "cancel"
            },
            //   { text: "OK", onPress: () => console.log("OK Pressed") }
        ]
    );
}
const Home = ({ navigation }) => {
    const [result, setResult] = React.useState<number | undefined>();
    const [mrzInput, setMrzInput] = React.useState({
        documentNumber: null,
        dateOfBirth: null,
        dateOfExpiry: null,
        nationality: null,
        gender: null,
        secondaryIdentifier: null,
        primaryIdentifier: null,
        issuingState: null
    });
    const [reading, setReading] = React.useState(false);

    const init = async () => {
        let mrz = await loadData("MRZ_DATA");
        console.log('init mrz: ', mrz);
        setMrzInput(mrz);
        setReading(false);

        MbReactNativeNfcInteract.addListener(NFC_READ_CARD_SUCCESS, (res: any) => {
            console.log(NFC_READ_CARD_SUCCESS + ":::::::::::::::", res);
            setReading(false);
            navigation.navigate('Detail', get(res, 'fromDevice.data', {}));
        }, (error) => {
            showAlert(JSON.stringify(error));
            setReading(false);
        });

        MbReactNativeNfcInteract.addListener(NFC_DISCOVERED, async (res: any) => {
            console.log(NFC_DISCOVERED + ":::::::::::::::", res);
            setReading(true);
            if (get(res, 'fromDevice.data.techList', []).includes("android.nfc.tech.IsoDep")) {
                const mrz = await loadData("MRZ_DATA");
                MbReactNativeNfcInteract.doReadCardIsodep(mrz.documentNumber, mrz.dateOfBirth, mrz.dateOfExpiry);
            } else {
                showAlert('Thiết bị không hỗ trợ đọc thông tin Chip');
            }

        }, (error) => {
            showAlert(JSON.stringify(error));
            setReading(false);
        }
        );


        MbReactNativeMrzJs.addListener(MRZ_DATA, (res: any) => {

            const mrzData = {
                ...res.fromDevice.data
                // documentNumber
                // nationality
                // dateOfExpiry
                // gender
                // dateOfBirth
                // secondaryIdentifier
                // primaryIdentifier
                // issuingState
            };
            console.log("MRZ_DATA:::::::::::::::", mrzData);

            setMrzInput(mrzData);
            storeData("MRZ_DATA", mrzData);
        });
    }

    const clean = () => {
        setMrzInput({});
        removeItem("MRZ_DATA");
        setReading(false);
    }

    React.useEffect(() => {
        init();
        setTimeout(() => {
            console.log('is enable :::::::::::::::"', MbReactNativeNfcInteract.isEnabled());
        }, 1000);

        return () => {
            setReading(false);
        };
    }, []);


    const doReadMrzInfo = () => {
        MbReactNativeMrzJs.doReadMrzInfo(ID_CARD);
    }

    const renderProcess = () => {
        // if (reading) {
        //     return (
        //         <Progress.Circle
        //             style={styles.progress}
        //             // progress={progress}
        //             // indeterminate={this.state.indeterminate}
        //             direction="counter-clockwise"
        //         />
        //     );
        // }

        return null;
    }

    const directionText = () => {
        if (reading && get(mrzInput, 'documentNumber', null)) {
            return "Đang đọc dữ liêu...";
        } else if (!reading && get(mrzInput, 'documentNumber', null)) {
            return "Vui lòng đưa CCCD lại gần thiện thoại và giữ trong giây lát... ";
        } else {
            return null;
        }

    }

    return (
        <View style={styles.container}>
            <View style={styles.info}>
                <Text style={styles.heading}>Thông tin đọc chuỗi MRZ </Text>
                <Text style={styles.text}>Số CCCD: {get(mrzInput, 'documentNumber', null)}</Text>
                <Text style={styles.text}>Quốc tịch: {get(mrzInput, 'nationality', null)}</Text>
                <Text style={styles.text}>Ngày cấp: {get(mrzInput, 'issuingState', null)}</Text>
                <Text style={styles.text}>Giới tính: {get(mrzInput, 'gender', null)}</Text>
                <Text style={styles.text}>Ngày sinh: {get(mrzInput, 'dateOfBirth', null)}</Text>
                <Text style={styles.text}>Giá trị đến: {get(mrzInput, 'dateOfExpiry', null)}</Text>
            </View>

            <View style={styles.scanning} >
                <Text style={styles.text}>{directionText()}</Text>
                {renderProcess()}
            </View>

            <View style={styles.groupButton} >
                <Button
                    onPress={doReadMrzInfo}
                    title="quét MRZ"
                    color="#841584"
                    accessibilityLabel="Quét MRZ"
                />

                <Button
                    onPress={clean}
                    title="Xoá dữ liệu"
                    color="#C1C5C4"
                    accessibilityLabel="Xoá dữ liệu"
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        backgroundColor: '#F8F2EE'
    },
    info: {
        width: '100%',
        height: '50%',
        marginVertical: 20,
    },
    groupButton: {
        width: '100%',
        height: '20%',
        flex: 1
    },
    scanning: {
        width: '100%',
        height: '30%',
        display: 'flex',
        alignItems: 'center',
    },
    button: {
        marginTop: 10
    },
    text: {
        fontSize: 16,
        marginTop: 10,
    },
    heading: {
        fontSize: 20,
        marginTop: 10,
        marginBottom: 10,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    circles: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    progress: {
        margin: 10,
    },
});

export default Home;