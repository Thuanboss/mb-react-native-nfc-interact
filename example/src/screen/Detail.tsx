import * as React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import { get } from 'lodash';

const Detail = ({ route, navigation, }) => {
    const [nfcData, setNfcData] = React.useState({ ...route.params });

    const init = (): void => {
        console.log(route.params);

        // "data": {
        //     "chipAuthSucceeded": false,
        //     "dateOfBirth": "940501",
        //     "dateOfExpiry": "340501",
        //     "documentCode": "ID",
        //     "documentNumber": "094003629",
        //     "gender": "MALE",
        //     "imageBase64": "",
        //     "issuingState": "VNM",
        //     "nationality": "VNM",
        //     "optionalData1": "006094003629  7",
        //     "optionalData2": "<<<<<<<<<<<",
        //     "passiveAuthSuccess": true,
        //     "personalNumber": "006094003629",
        //     "primaryIdentifier": "MA",
        //     "secondaryIdentifier": "DUC<THUAN<<<<<<<<<<<<<<<<<"
        // },
    }

    const clean = (): void => {

    }

    React.useEffect(() => {
        init();

        return clean;
    }, []);

    return (
        <View style={styles.container}>
            <Text style={styles.heading}>Thông tin đọc từ chip </Text>

            <View style={styles.avatar}>
                <Image
                    source={{
                        uri: `data:image/jpeg;base64,${get(nfcData, 'imageBase64', null)}`,
                    }}
                    style={styles.image}
                />
            </View>
            <View style={styles.info}>
                <Text style={styles.text}>Loại GTTT : {get(nfcData, 'documentCode', null)}</Text>
                <Text style={styles.text}>Số CCCD: {get(nfcData, 'personalNumber', null)}</Text>
                <Text style={styles.text}>Họ Tên: {get(nfcData, 'primaryIdentifier', null)} {get(nfcData, 'secondaryIdentifier', null)}</Text>
                <Text style={styles.text}>Quốc tịch: {get(nfcData, 'nationality', null)}</Text>
                {/* <Text style={styles.text}>Ngày cấp: {get(nfcData, 'issuingState', null)}</Text> */}
                <Text style={styles.text}>Giới tính: {get(nfcData, 'gender', null)}</Text>
                <Text style={styles.text}>Ngày sinh: {get(nfcData, 'dateOfBirth', null)}</Text>
                <Text style={styles.text}>Giá trị đến: {get(nfcData, 'dateOfExpiry', null)}</Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        backgroundColor: '#F8F2EE'
    },
    info: {
        width: '100%',
        height: '40%',
        marginVertical: 20,
    },
    button: {
        marginTop: 10
    },
    text: {
        fontSize: 16,
        marginTop: 10,
    },
    heading: {
        fontSize: 20,
        marginTop: 10,
        marginBottom: 10,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    circles: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    progress: {
        margin: 10,
    },
    avatar: {
        width: '100%',
        height: '60%',
        flex: 1,
        alignItems: 'center',
    },
    image: {
        width: '70%',
        height: '100%',
    }
});


export default Detail;