package com.reactnativembreactnativemrz.mrz.parser;

import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import com.reactnativembreactnativemrz.mrz.utils.Constant;

public class ExceptionParser {

    public static WritableMap parse(Exception ex) {
        WritableMap result = new WritableNativeMap();
        result.putString("status", Constant.EVENT.MRZ_ERROR);
        result.putString("message", ex.getMessage());
        result.putString("localizedMessage", ex.getLocalizedMessage());
        return result;
    }

  public static WritableMap requirePermission(String message, String code) {
    WritableMap result = new WritableNativeMap();
    result.putString("code", code);
    result.putString("message", message);
    return result;
  }
}
