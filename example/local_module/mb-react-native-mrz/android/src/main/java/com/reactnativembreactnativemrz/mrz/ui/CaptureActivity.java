package com.reactnativembreactnativemrz.mrz.ui;


import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;

import com.facebook.react.ReactActivity;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.reactnativembreactnativemrz.R;
import com.reactnativembreactnativemrz.mrz.dto.DocType;
import com.reactnativembreactnativemrz.mrz.mlkit.camera.CameraSource;
import com.reactnativembreactnativemrz.mrz.mlkit.camera.CameraSourcePreview;
import com.reactnativembreactnativemrz.mrz.mlkit.other.GraphicOverlay;
import com.reactnativembreactnativemrz.mrz.mlkit.text.TextRecognitionProcessor;
import com.reactnativembreactnativemrz.mrz.parser.ExceptionParser;
import com.reactnativembreactnativemrz.mrz.parser.MrzParser;
import com.reactnativembreactnativemrz.mrz.utils.Constant;

import org.jmrtd.lds.icao.MRZInfo;
import java.io.IOException;
import javax.annotation.Nullable;

public class CaptureActivity extends ReactActivity implements TextRecognitionProcessor.ResultListener {

  private CameraSource cameraSource = null;
  private CameraSourcePreview preview;
  private GraphicOverlay graphicOverlay;
  private ActionBar actionBar;
  private DocType docType = DocType.OTHER;
  private static String TAG = CaptureActivity.class.getSimpleName();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_capture);
    actionBar = getSupportActionBar();

    if (actionBar != null) {
      actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
      actionBar.setCustomView(R.layout.action_bar_title);
    }

    if (getIntent().hasExtra(Constant.DOC_TYPE)) {

      try {
        docType = (DocType) getIntent().getSerializableExtra(Constant.DOC_TYPE);

        if (docType == DocType.PASSPORT) {
          actionBar.hide();
          setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
      } catch (Exception e) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constant.MRZ_RESULT, e.getMessage());
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
      }
    }

    preview = findViewById(R.id.camera_source_preview);

    if (preview == null) {
      sendLog("Preview is null");
    }

    graphicOverlay = findViewById(R.id.graphics_overlay);

    if (graphicOverlay == null) {
      sendLog("graphicOverlay is null");
    }

    createCameraSource();
    startCameraSource();
  }

  @Override
  public void onResume() {
    super.onResume();
    sendLog("onResume");
    startCameraSource();
  }

  /**
   * Stops the camera.
   */
  @Override
  protected void onPause() {
    super.onPause();
  }

  @Override
  protected void onStop() {
    super.onStop();
    finish();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();

    if (cameraSource != null) {
      cameraSource.release();
      cameraSource = null;
    }
  }

  private void createCameraSource() {

    if (cameraSource == null) {
      cameraSource = new CameraSource(this, graphicOverlay);
      cameraSource.setFacing(CameraSource.CAMERA_FACING_BACK);
    }

    cameraSource.setMachineLearningFrameProcessor(new TextRecognitionProcessor(docType, this));
  }

  private void startCameraSource() {
    if (cameraSource != null) {
      try {
        if (preview == null) {
          sendLog("resume: Preview is null");
        }
        if (graphicOverlay == null) {
          sendLog("resume: graphOverlay is null");
        }
        preview.start(cameraSource, graphicOverlay);
      } catch (IOException e) {
        sendLog("Unable to start camera source." + e.getMessage());
        cameraSource.release();
        cameraSource = null;
      }
    }
  }

  @Override
  public void onSuccess(MRZInfo mrzInfo) {
    sendResponseEvent(Constant.EVENT.MRZ_DATA, MrzParser.parse(mrzInfo));

    try {
      finish();
    } catch (Exception e) {
      sendLog("onSuccess cameraSource " + e.getMessage());
    }
  }

  @Override
  public void onError(Exception exp) {
    sendResponseEvent(Constant.EVENT.MRZ_ERROR, ExceptionParser.parse(exp));

    try {
      finish();
    } catch (Exception e) {
      sendLog("onError cameraSource " + e.getMessage());
    }
  }

  private void sendLog(String mess) {
    WritableMap payloadLog = new WritableNativeMap();
    payloadLog.putString("message", mess);
    sendResponseEvent(Constant.EVENT.MRZ_LOGGING, payloadLog);
  }

  private void sendResponseEvent(String event, @Nullable WritableMap payload) {

    if (payload != null) {
      payload.putString("origin", "android");
    }

    getReactInstanceManager().getCurrentReactContext()
      .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
      .emit(event, payload);
  }
}

