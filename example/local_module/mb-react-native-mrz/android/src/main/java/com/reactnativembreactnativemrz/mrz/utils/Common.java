package com.reactnativembreactnativemrz.mrz.utils;

import android.widget.EditText;

import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableMapKeySetIterator;
import com.facebook.react.bridge.ReadableType;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeArray;
import com.facebook.react.bridge.WritableNativeMap;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Common {

    public static Calendar loadDate(EditText editText) {
        Calendar calendar = Calendar.getInstance();

        if (!editText.getText().toString().isEmpty()) {
            try {
                calendar.setTimeInMillis(new SimpleDateFormat("yyyy-MM-dd", Locale.US)
                        .parse(editText.getText().toString()).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return calendar;
    }


    public static WritableMap cloneWritableMap(WritableMap map) {
        if (map == null) {
            return null;
        }
        WritableMap r = new WritableNativeMap();

        ReadableMapKeySetIterator iter = map.keySetIterator();
        while (iter.hasNextKey()) {
            String key = iter.nextKey();
            ReadableType type = map.getType(key);
            switch (type) {
                case Array:
                    r.putArray(key, cloneReadableArray(map.getArray(key)));
                    break;
                case Boolean:
                    r.putBoolean(key, map.getBoolean(key));
                    break;
                case Map:
                    r.putMap(key, cloneReadableMap(map.getMap(key)));
                    break;
                case Null:
                    r.putNull(key);
                    break;
                case Number:
                    r.putDouble(key, map.getDouble(key));
                    break;
                case String:
                    r.putString(key, map.getString(key));
                    break;
            }
        }
        return r;
    }


    public static WritableMap cloneReadableMap(ReadableMap map) {
        if (map == null) {
            return null;
        }
        WritableMap r = new WritableNativeMap();

        ReadableMapKeySetIterator iter = map.keySetIterator();
        while (iter.hasNextKey()) {
            String key = iter.nextKey();
            ReadableType type = map.getType(key);
            switch (type) {
                case Array:
                    r.putArray(key, cloneReadableArray(map.getArray(key)));
                    break;
                case Boolean:
                    r.putBoolean(key, map.getBoolean(key));
                    break;
                case Map:
                    r.putMap(key, cloneReadableMap(map.getMap(key)));
                    break;
                case Null:
                    r.putNull(key);
                    break;
                case Number:
                    r.putDouble(key, map.getDouble(key));
                    break;
                case String:
                    r.putString(key, map.getString(key));
                    break;
            }
        }
        return r;
    }


    public static WritableArray cloneWritableArray(WritableArray arr) {
        if (arr == null) {
            return null;
        }
        WritableArray r = new WritableNativeArray();

        for (int i = 0; i < arr.size(); i++) {
            ReadableType type = arr.getType(i);
            switch (type) {
                case Array:
                    r.pushArray(cloneReadableArray(arr.getArray(i)));
                    break;
                case Boolean:
                    r.pushBoolean(arr.getBoolean(i));
                    break;
                case Map:
                    r.pushMap(cloneReadableMap(arr.getMap(i)));
                    break;
                case Null:
                    r.pushNull();
                    break;
                case Number:
                    r.pushDouble(arr.getDouble(i));
                    break;
                case String:
                    r.pushString(arr.getString(i));
                    break;
            }
        }

        return r;
    }


    public static WritableArray cloneReadableArray(ReadableArray arr) {
        if (arr == null) {
            return null;
        }
        WritableArray r = new WritableNativeArray();

        for (int i = 0; i < arr.size(); i++) {
            ReadableType type = arr.getType(i);
            switch (type) {
                case Array:
                    r.pushArray(cloneReadableArray(arr.getArray(i)));
                    break;
                case Boolean:
                    r.pushBoolean(arr.getBoolean(i));
                    break;
                case Map:
                    r.pushMap(cloneReadableMap(arr.getMap(i)));
                    break;
                case Null:
                    r.pushNull();
                    break;
                case Number:
                    r.pushDouble(arr.getDouble(i));
                    break;
                case String:
                    r.pushString(arr.getString(i));
                    break;
            }
        }

        return r;
    }

}
