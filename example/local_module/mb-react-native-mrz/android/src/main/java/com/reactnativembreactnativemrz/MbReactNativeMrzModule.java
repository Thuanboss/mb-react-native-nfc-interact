package com.reactnativembreactnativemrz;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.reactnativembreactnativemrz.mrz.dto.DocType;
import com.reactnativembreactnativemrz.mrz.parser.ExceptionParser;
import com.reactnativembreactnativemrz.mrz.ui.CaptureActivity;
import com.reactnativembreactnativemrz.mrz.utils.Constant;
import com.reactnativembreactnativemrz.mrz.utils.PermissionUtil;

@ReactModule(name = MbReactNativeMrzModule.NAME)
public class MbReactNativeMrzModule extends ReactContextBaseJavaModule implements ActivityEventListener, LifecycleEventListener {
  public static final String NAME = "MbReactNativeMrz";
  private static final int APP_CAMERA_ACTIVITY_REQUEST_CODE = 150;
  private static final int APP_SETTINGS_ACTIVITY_REQUEST_CODE = 550;

  public MbReactNativeMrzModule(ReactApplicationContext reactContext) {
    super(reactContext);
    reactContext.addActivityEventListener(this);
    reactContext.addLifecycleEventListener(this);
  }

  @Override
  @NonNull
  public String getName() {
    return NAME;
  }


  @Override
  public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {

  }

  @Override
  public void onNewIntent(Intent intent) {

  }

  @Override
  public void onHostResume() {

  }

  @Override
  public void onHostPause() {
  }

  @Override
  public void onHostDestroy() {

  }

  private void sendResponseEvent(String event, @Nullable WritableMap payload) {

    if (payload != null) {
      payload.putString("origin", "android");
    }

    getReactApplicationContext()
      .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
      .emit(event, payload);
  }

  private void sendLog(String mess) {
    WritableMap payloadLog = new WritableNativeMap();
    payloadLog.putString("message", mess);
    sendResponseEvent(Constant.EVENT.MRZ_LOGGING, payloadLog);
  }

  private void requestPermissionForCamera(DocType docType) {
    String[] permissions = {Manifest.permission.CAMERA};
    boolean isPermissionGranted = PermissionUtil.hasPermissions(getCurrentActivity(), permissions);

    if (!isPermissionGranted) {
      sendResponseEvent(Constant.EVENT.MRZ_REQUIRE_PERMISSION, ExceptionParser.requirePermission("Require permission camera!", Manifest.permission.CAMERA));
    } else {
      openCameraActivity(docType);
    }
  }

  private void openCameraActivity(DocType docType) {

    try {
      Intent intent = new Intent(getCurrentActivity(), CaptureActivity.class);
      intent.putExtra(Constant.DOC_TYPE, docType);
      getCurrentActivity().startActivityForResult(intent, APP_CAMERA_ACTIVITY_REQUEST_CODE, null);
    } catch (Exception e) {
      sendResponseEvent(Constant.EVENT.MRZ_ERROR, ExceptionParser.parse(e));
    }
  }

  @ReactMethod
  public void doReadMrzInfo(String type) {
    sendLog("doReadMrzInfo " + type);

    switch (type) {
      case Constant.ID_CARD:
        requestPermissionForCamera(DocType.ID_CARD);
        break;
      case Constant.PASSPORT:
        requestPermissionForCamera(DocType.PASSPORT);
        break;
      default:
        break;
    }
  }

}
