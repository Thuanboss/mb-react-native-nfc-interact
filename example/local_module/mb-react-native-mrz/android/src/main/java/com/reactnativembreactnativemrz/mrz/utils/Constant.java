package com.reactnativembreactnativemrz.mrz.utils;

public class Constant {
  public static interface EVENT {
    String MRZ_ERROR = "MRZ_ERROR";
    String MRZ_LOGGING = "MRZ_LOGGING";
    String MRZ_DATA = "MRZ_DATA";
    String MRZ_REQUIRE_PERMISSION = "MRZ_REQUIRE_PERMISSION";
  }

  public static final String MRZ_RESULT = "MRZ_RESULT";
  public static final String DOC_TYPE = "DOC_TYPE";
  public static final String ID_CARD= "ID_CARD";
  public static final String PASSPORT = "PASSPORT";
}
