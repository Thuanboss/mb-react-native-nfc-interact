package com.reactnativembreactnativemrz.mrz.parser;

import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;

import org.jmrtd.lds.icao.MRZInfo;

public class MrzParser {

  public static WritableMap parse(MRZInfo mrzInfo) {
    WritableMap result = new WritableNativeMap();
    WritableMap data = new WritableNativeMap();
    data.putString("documentCode", mrzInfo.getDocumentCode());
    data.putString("documentNumber", mrzInfo.getDocumentNumber());
    data.putString("nationality", mrzInfo.getNationality());
    data.putString("dateOfExpiry", mrzInfo.getDateOfExpiry());
    data.putString("gender", mrzInfo.getGender().toString());
    data.putString("dateOfBirth", mrzInfo.getDateOfBirth());
    data.putString("secondaryIdentifier", mrzInfo.getSecondaryIdentifier());
    data.putString("primaryIdentifier", mrzInfo.getPrimaryIdentifier());
    data.putString("issuingState", mrzInfo.getIssuingState());
    result.putMap("data", data);
    return result;
  }

}
