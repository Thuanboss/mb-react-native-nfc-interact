package com.reactnativembreactnativemrz.mrz.dto;

public enum DocType {
    PASSPORT, ID_CARD, OTHER
}
