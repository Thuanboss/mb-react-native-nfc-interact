'use strict';

import {
    NativeModules,
    DeviceEventEmitter,
    NativeEventEmitter,
    Platform,
    PermissionsAndroid
} from 'react-native';

const LINKING_ERROR =
    `The package 'mb-react-native-mrz' doesn't seem to be linked. Make sure: \n\n` +
    Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
    '- You rebuilt the app after installing the package\n' +
    '- You are not using Expo managed workflow\n';

const MbReactNativeMrz = NativeModules.MbReactNativeMrz
    ? NativeModules.MbReactNativeMrz
    : new Proxy(
        {},
        {
            get() {
                throw new Error(LINKING_ERROR);
            },
        }
    );

const eventEmitter = new NativeEventEmitter(MbReactNativeMrz);
export const MRZ_ERROR = "MRZ_ERROR";
export const MRZ_LOGGING = "MRZ_LOGGING";
export const MRZ_DATA = "MRZ_DATA";
export const MRZ_REQUIRE_PERMISSION = "MRZ_REQUIRE_PERMISSION";
export const ID_CARD = 'ID_CARD';
export const PASSPORT = 'PASSPORT';

let _listeners: any = {};
let _errListeners: any = {};

const _notifyErrListeners = (data: any) => {
    if (data) {
        for (let _listener in _errListeners) {
            _errListeners[_listener](data);
        }
    }
};

const onMrzData = (data: any) => {
    let payload: any = {
        fromDevice: data,
        id: null,
        origin: null,
        encoding: null,
        scanned: null
    };

    payload.origin = data.origin;
    payload.id = data.id;

    if (Platform.OS !== "ios" && data.type == "TAG") {
        payload.fromDevice.data = [[data.data]]
        payload.encoding = "UTF-8";
        payload.scanned = data.id
    } else {
        if (data.data[0] && data.data[0][0]) {
            let dat = data.data[0][0];
            payload.encoding = dat.encoding;
            payload.scanned = dat.data;
        }
    }

    if (data) {
        for (let _listener in _listeners) {
            console.log("_listener: ", _listener);

            if (_listener === MRZ_DATA) {
                _listeners[_listener](payload);
                break;
            }
        }
    }
}

if (Platform.OS == "ios") {
    DeviceEventEmitter.removeAllListeners(MRZ_LOGGING);
    DeviceEventEmitter.removeAllListeners(MRZ_REQUIRE_PERMISSION);


} else {
    DeviceEventEmitter.removeAllListeners(MRZ_LOGGING);
    DeviceEventEmitter.removeAllListeners(MRZ_REQUIRE_PERMISSION);

    DeviceEventEmitter.addListener(MRZ_LOGGING, (res) => {
        console.log("MRZ_LOGGING:::::::::::::::::::::::", res);
    });

    DeviceEventEmitter.addListener(MRZ_REQUIRE_PERMISSION, async (res) => {
        console.log("MRZ_REQUIRE_PERMISSION:::::::::::::::::::::::", res);

        const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA, {
            title: "MB React Native MRZ require camera Perimision",
            message: "MB React Native MRZ need access to your camera",
            buttonNeutral: "Ask me laster",
            buttonNegative: "Cancel",
            buttonPositive: "OK",
        })

        console.log("granted:::::::::", granted);
    });
}

const _registerToEvents = () => {
    if (Platform.OS == "ios") {
        try {
            DeviceEventEmitter.removeAllListeners(MRZ_ERROR);
            DeviceEventEmitter.removeAllListeners(MRZ_DATA);
            DeviceEventEmitter.addListener(MRZ_ERROR, _notifyErrListeners);
            DeviceEventEmitter.addListener(MRZ_DATA, onMrzData);
        } catch (iosErr) {
            console.log("iosErr", iosErr);
        }
    } else {
        try {
            DeviceEventEmitter.removeAllListeners(MRZ_ERROR);
            DeviceEventEmitter.removeAllListeners(MRZ_DATA);
            DeviceEventEmitter.addListener(MRZ_ERROR, _notifyErrListeners);
            DeviceEventEmitter.addListener(MRZ_DATA, onMrzData);
        } catch (androidErr) {
            console.log("androidErr", androidErr);
        }
    }
};

const MbReactNativeMrzJs: any = {};

MbReactNativeMrzJs.doReadMrzInfo = (type: String) => {
    MbReactNativeMrz.doReadMrzInfo(type);
}

MbReactNativeMrzJs.addListener = (name: any, callback: any, error: any) => {
    if (callback) {
        _listeners[name] = callback;
    }
    if (error) {
        _errListeners[name] = error;
    }
    _registerToEvents();
};

MbReactNativeMrzJs.removeListener = (name: any) => {
    delete _listeners[name];
    delete _errListeners[name];
};

MbReactNativeMrzJs.removeAllListeners = () => {
    if (Platform.OS == "ios") {
        try {
            eventEmitter.removeAllListeners(MRZ_ERROR);
            eventEmitter.removeAllListeners(MRZ_DATA);
        } catch (iosErr) {
            console.log("iosErr", iosErr);
        }
    }
    else {
        try {
            eventEmitter.removeAllListeners(MRZ_ERROR);
            eventEmitter.removeAllListeners(MRZ_DATA);
        } catch (androidErr) {
            console.log("androidErr", androidErr);
        }
    }
    _listeners = {};
    _errListeners = {};
};

export default MbReactNativeMrzJs;