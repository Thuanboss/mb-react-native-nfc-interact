# react-native-mb-react-native-mrz

a

## Installation

```sh
npm install react-native-mb-react-native-mrz
```

## Usage

```js
import { multiply } from "react-native-mb-react-native-mrz";

// ...

const result = await multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
