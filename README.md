# mb-react-native-nfc-interact

a

## Installation

```sh
npm install mb-react-native-nfc-interact
```

## Usage

```js
import { multiply } from "mb-react-native-nfc-interact";

// ...

const result = await multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
