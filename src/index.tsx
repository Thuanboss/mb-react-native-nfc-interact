'use strict';

import {
    NativeModules,
    DeviceEventEmitter,
    NativeEventEmitter,
    Platform
} from 'react-native';

const LINKING_ERROR =
    `The package 'mb-react-native-nfc-interact' doesn't seem to be linked. Make sure: \n\n` +
    Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
    '- You rebuilt the app after installing the package\n' +
    '- You are not using Expo managed workflow\n';

const MbReactNativeNfcInteract = NativeModules.MbReactNativeNfcInteract
    ? NativeModules.MbReactNativeNfcInteract
    : new Proxy(
        {},
        {
            get() {
                throw new Error(LINKING_ERROR);
            },
        }
    );

const eventEmitter = new NativeEventEmitter(MbReactNativeNfcInteract);
export const NFC_DISCOVERED = 'NFC_DISCOVERED';
export const NFC_ERROR = 'NFC_ERROR';
export const NFC_MISSING = 'NFC_MISSING';
export const NFC_UNAVAILABLE = 'NFC_UNAVAILABLE';
export const NFC_ENABLED = 'NFC_ENABLED';
export const NFC_READ_CARD_SUCCESS = 'NFC_READ_CARD_SUCCESS';
export const NFC_LOGGING = 'NFC_LOGGING';

export const ID_CARD = 'ID_CARD';
export const PASSPORT = 'PASSPORT';

// const startUpNfcData = ReactNativeNFC.getStartUpNfcData || (() => ({}));

let _enabled = true;
let _listeners: any = {};
let _errListeners: any = {};
let _loading = true;
let _status = "waiting";

const onNfcDiscovered = (data: any) => {
    let payload: any = {
        fromDevice: data,
        id: null,
        type: null,
        origin: null,
        encoding: null,
        scanned: null
    };

    payload.origin = data.origin;
    payload.id = data.id;
    payload.type = data.type;

    if (Platform.OS !== "ios" && data.type == "TAG") {
        let tagType = data.data.techList[0]
        payload.fromDevice.data = [[data.data]]
        payload.encoding = "UTF-8";
        payload.type = tagType.replace("android.nfc.tech.", "")
        payload.scanned = data.id
    } else {
        if (data.data[0] && data.data[0][0]) {
            let dat = data.data[0][0];
            payload.encoding = dat.encoding;
            payload.scanned = dat.data;
        }
    }

    if (data) {
        for (let _listener in _listeners) {
            console.log("_listener: ", _listener);

            if (_listener === NFC_DISCOVERED) {
                _listeners[_listener](payload);
                break;
            }
        }
    }
};

const _notifyErrListeners = (data: any) => {

    console.log('_notifyErrListeners', data);

    if (data) {
        for (let _listener in _errListeners) {
            _errListeners[_listener](data);
        }
    }
};

const onReadCardSuccess = (data: any) => {
    let payload: any = {
        fromDevice: data,
        id: null,
        type: null,
        origin: null,
        encoding: null,
        scanned: null
    };

    payload.origin = data.origin;
    payload.id = data.id;
    payload.type = data.type;

    if (Platform.OS !== "ios" && data.type == "TAG") {
        let tagType = data.data.techList[0]
        payload.fromDevice.data = [[data.data]]
        payload.encoding = "UTF-8";
        payload.type = tagType.replace("android.nfc.tech.", "")
        payload.scanned = data.id
    } else {
        if (data.data[0] && data.data[0][0]) {
            let dat = data.data[0][0];
            payload.encoding = dat.encoding;
            payload.scanned = dat.data;
        }
    }

    if (data) {
        for (let _listener in _listeners) {
            console.log("_listener: ", _listener);

            if (_listener === NFC_READ_CARD_SUCCESS) {
                _listeners[_listener](payload);
                break;
            }
        }
    }
}

if (Platform.OS == "ios") {
    eventEmitter.addListener(NFC_MISSING, () => { _enabled = false; _loading = false; _status = "missing"; });
    eventEmitter.addListener(NFC_UNAVAILABLE, () => { _enabled = false; _loading = false; _status = "unavailable"; });
    eventEmitter.addListener(NFC_ENABLED, () => { _enabled = true; _loading = false; _status = "ready"; });
} else {

    DeviceEventEmitter.addListener(NFC_MISSING, (res) => {
        console.log("NFC_MISSING:::::::::::::::::::::::", res);
        _enabled = false;
        _loading = false;
        _status = "missing";
    });

    DeviceEventEmitter.addListener(NFC_UNAVAILABLE, (res) => {
        console.log("NFC_UNAVAILABLE:::::::::::::::::::::::", res);
        _enabled = false;
        _loading = false;
        _status = "unavailable";
    });

    DeviceEventEmitter.addListener(NFC_ENABLED, (res) => {
        console.log("NFC_ENABLED:::::::::::::::::::::::", res);
        _enabled = true;
        _loading = false;
        _status = "ready";
    });

    DeviceEventEmitter.addListener(NFC_LOGGING, (res) => {
        console.log("NFC_LOGGING:::::::::::::::::::::::", res);
    });
}

MbReactNativeNfcInteract.initialize();

const _registerToEvents = () => {
    if (Platform.OS == "ios") {
        try {
            DeviceEventEmitter.removeAllListeners(NFC_DISCOVERED);
            DeviceEventEmitter.removeAllListeners(NFC_ERROR);
            DeviceEventEmitter.removeAllListeners(NFC_READ_CARD_SUCCESS);
            DeviceEventEmitter.addListener(NFC_DISCOVERED, onNfcDiscovered);
            DeviceEventEmitter.addListener(NFC_ERROR, _notifyErrListeners);
            DeviceEventEmitter.addListener(NFC_READ_CARD_SUCCESS, onReadCardSuccess);
        } catch (iosErr) {
            console.log("iosErr", iosErr);
        }
    } else {
        try {
            DeviceEventEmitter.removeAllListeners(NFC_DISCOVERED);
            DeviceEventEmitter.removeAllListeners(NFC_ERROR);
            DeviceEventEmitter.removeAllListeners(NFC_READ_CARD_SUCCESS);
            DeviceEventEmitter.addListener(NFC_DISCOVERED, onNfcDiscovered);
            DeviceEventEmitter.addListener(NFC_ERROR, _notifyErrListeners);
            DeviceEventEmitter.addListener(NFC_READ_CARD_SUCCESS, onReadCardSuccess);

        } catch (androidErr) {
            console.log("androidErr", androidErr);
        }
    }
};

const MbNFCInteract: any = {};

MbNFCInteract.initialize = () => {
    const init = MbReactNativeNfcInteract.initialize || (() => ({}));
    if (_enabled && !_loading) {
        init();
    } else {
        throw new Error("NFC Controller object is not ready");
    }
};

// MbNFCInteract.stopScan = () => {
//     const stopScan = MbReactNativeNfcInteract.stopScan || (() => ({}));
//     stopScan();
// };

MbNFCInteract.isEnabled = () => {
    return _enabled && !_loading;
};


MbNFCInteract.checkDeviceStatus = () => {
    return _status;
}

MbNFCInteract.doReadCardIsodep = (idNumber: String, dateOfBirth: String, dateOfExpiry: String) => {
    MbReactNativeNfcInteract.doReadCardIsodep(idNumber, dateOfBirth, dateOfExpiry);
}

MbNFCInteract.doReadMrzInfo = (type: String) => {
    MbReactNativeNfcInteract.doReadMrzInfo(type);
}

MbNFCInteract.addListener = (name: any, callback: any, error: any) => {
    if (callback) {
        _listeners[name] = callback;
    }
    if (error) {
        _errListeners[name] = error;
    }
    _registerToEvents();
};

MbNFCInteract.removeListener = (name: any) => {
    delete _listeners[name];
    delete _errListeners[name];
};

MbNFCInteract.removeAllListeners = () => {
    if (Platform.OS == "ios") {
        try {
            eventEmitter.removeAllListeners(NFC_DISCOVERED);
            eventEmitter.removeAllListeners(NFC_ERROR);
            eventEmitter.removeAllListeners(NFC_READ_CARD_SUCCESS);
        } catch (iosErr) {
            console.log("iosErr", iosErr);
        }
    }
    else {
        try {
            DeviceEventEmitter.removeAllListeners(NFC_DISCOVERED);
            DeviceEventEmitter.removeAllListeners(NFC_ERROR);
            DeviceEventEmitter.removeAllListeners(NFC_READ_CARD_SUCCESS);
        } catch (androidErr) {
            console.log("androidErr", androidErr);
        }
    }
    _listeners = {};
    _errListeners = {};
};

export default MbNFCInteract;