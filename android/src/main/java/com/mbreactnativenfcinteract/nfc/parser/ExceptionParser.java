package com.mbreactnativenfcinteract.nfc.parser;

import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import com.mbreactnativenfcinteract.nfc.utils.Constant;

public class ExceptionParser {

    public static WritableMap parse(Exception ex) {
        WritableMap result = new WritableNativeMap();
        result.putString("status", Constant.EVENT.NFC_ERROR);
        result.putString("message", ex.getMessage());
        result.putString("localizedMessage", ex.getLocalizedMessage());
        return result;
    }
}
