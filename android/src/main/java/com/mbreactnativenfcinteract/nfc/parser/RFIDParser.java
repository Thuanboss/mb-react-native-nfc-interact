package com.mbreactnativenfcinteract.nfc.parser;//package com.mb.react.nfcinteract.nfc.parser;

import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;

import org.jmrtd.lds.icao.MRZInfo;

public class RFIDParser {

    public static WritableMap parse(MRZInfo mrzInfo, String imageBase64, boolean chipAuthSucceeded, boolean passiveAuthSuccess){
        WritableMap result = new WritableNativeMap();
        result.putString("id", "test");
        WritableMap data = new WritableNativeMap();
        data.putString("imageBase64", imageBase64);
        data.putBoolean("chipAuthSucceeded", chipAuthSucceeded);
        data.putBoolean("passiveAuthSuccess", passiveAuthSuccess);
        data.putString("documentCode", mrzInfo.getDocumentCode());
        data.putString("issuingState", mrzInfo.getIssuingState());
        data.putString("primaryIdentifier", mrzInfo.getPrimaryIdentifier());
        data.putString("secondaryIdentifier", mrzInfo.getSecondaryIdentifier());
        data.putString("nationality", mrzInfo.getNationality());
        data.putString("documentNumber", mrzInfo.getDocumentNumber());
        data.putString("dateOfBirth", mrzInfo.getDateOfBirth());
        data.putString("gender", String.valueOf(mrzInfo.getGender()));
        data.putString("dateOfExpiry", mrzInfo.getDateOfExpiry());
        data.putString("optionalData1", mrzInfo.getOptionalData1());
        data.putString("optionalData2", mrzInfo.getOptionalData2());
        data.putString("personalNumber", mrzInfo.getPersonalNumber());
        result.putMap("data", data);
        return result;
    }
}
