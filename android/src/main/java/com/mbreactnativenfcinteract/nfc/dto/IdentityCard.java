package com.mbreactnativenfcinteract.nfc.dto;

public class IdentityCard {
    private String dateOfBirth;
    private String dateOfExpiry;
    private String identityNumber;
    private String photoAsBase64;

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getDateOfExpiry() {
        return dateOfExpiry;
    }

    public void setDateOfExpiry(String dateOfExpiry) {
        this.dateOfExpiry = dateOfExpiry;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getPhotoAsBase64() {
        return photoAsBase64;
    }

    public void setPhotoAsBase64(String photoAsBase64) {
        this.photoAsBase64 = photoAsBase64;
    }
}
