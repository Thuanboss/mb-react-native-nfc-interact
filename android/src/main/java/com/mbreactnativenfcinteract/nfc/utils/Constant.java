package com.mbreactnativenfcinteract.nfc.utils;

public class Constant {
  public final static String KEY_PASSPORT_NUMBER = "passportNumber";
  public final static String KEY_EXPIRATION_DATE = "expirationDate";
  public final static String KEY_BIRTH_DATE = "birthDate";

  public static interface EVENT {
    String NFC_ERROR = "NFC_ERROR";
    String NFC_ENABLED = "NFC_ENABLED";
    String NFC_DISCOVERED = "NFC_DISCOVERED";
    String NFC_MISSING = "NFC_MISSING";
    String NFC_UNAVAILABLE = "NFC_UNAVAILABLE";
    String NFC_READ_CARD_SUCCESS = "NFC_READ_CARD_SUCCESS";
    String NFC_LOGGING = "NFC_LOGGING";
  }
}
