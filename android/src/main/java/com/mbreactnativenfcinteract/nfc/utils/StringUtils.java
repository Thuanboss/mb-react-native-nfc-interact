package com.mbreactnativenfcinteract.nfc.utils;//package com.mb.react.nfcinteract.nfc.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class StringUtils {
    public static String objectToJson(Object obj) {

        try {
            if (obj == null) {
                return null;
            }

            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            return null;
        }
    }

    public static <T> T jsonToObject(String json, Class<T> clazz) {

        try {
            if (stringIsNullOrEmpty(json)) {
                return null;
            }

            ObjectMapper objectMapper = new ObjectMapper();
            T result = objectMapper.readValue(json, clazz);
            return result;
        } catch (Exception e) {
            System.out.println("JSON to object error!" + e.getMessage());
            return null;
        }
    }

    public static boolean stringIsNullOrEmpty(String str) {
        if (str == null)
            return true;
        else {
            if (str.trim().length() <= 0)
                return true;
        }
        return false;
    }

}
