package com.mbreactnativenfcinteract;

import static org.jmrtd.PassportService.DEFAULT_MAX_BLOCKSIZE;
import static org.jmrtd.PassportService.NORMAL_MAX_TRANCEIVE_LENGTH;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcB;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.mbreactnativenfcinteract.nfc.parser.ExceptionParser;
import com.mbreactnativenfcinteract.nfc.parser.RFIDParser;
import com.mbreactnativenfcinteract.nfc.parser.TagParser;
import com.mbreactnativenfcinteract.nfc.utils.Common;
import com.mbreactnativenfcinteract.nfc.utils.Constant;
import com.mbreactnativenfcinteract.nfc.utils.ImageUtil;
import com.mbreactnativenfcinteract.nfc.utils.StringUtils;

import net.sf.scuba.smartcards.CardFileInputStream;
import net.sf.scuba.smartcards.CardService;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.x509.Certificate;
import org.jmrtd.BACKey;
import org.jmrtd.BACKeySpec;
import org.jmrtd.PassportService;
import org.jmrtd.lds.CardAccessFile;
import org.jmrtd.lds.ChipAuthenticationPublicKeyInfo;
import org.jmrtd.lds.PACEInfo;
import org.jmrtd.lds.SODFile;
import org.jmrtd.lds.SecurityInfo;
import org.jmrtd.lds.icao.DG14File;
import org.jmrtd.lds.icao.DG1File;
import org.jmrtd.lds.icao.DG2File;
import org.jmrtd.lds.icao.MRZInfo;
import org.jmrtd.lds.iso19794.FaceImageInfo;
import org.jmrtd.lds.iso19794.FaceInfo;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.CertPath;
import java.security.cert.CertPathValidator;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXParameters;
import java.security.cert.X509Certificate;
import java.security.spec.MGF1ParameterSpec;
import java.security.spec.PSSParameterSpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;


public class MbReactNativeNfcInteractModule extends ReactContextBaseJavaModule implements ActivityEventListener, LifecycleEventListener {
  public static final String NAME = "MbReactNativeNfcInteract";

  private NfcAdapter nfcAdapter;
  private static final String[][] techList = new String[][]{new String[]{
    IsoDep.class.getName(),
    NfcA.class.getName(),
    NfcB.class.getName(),
    NfcF.class.getName(),
    NfcV.class.getName(),
    Ndef.class.getName(),
//    NfcBarcode.class.getName(),
    NdefFormatable.class.getName(),
    MifareClassic.class.getName(),
    MifareUltralight.class.getName()
  }};

  private WritableMap startupNfcData;
  private boolean startupNfcDataRetrieved = false;
  private Tag tag;

  public MbReactNativeNfcInteractModule(ReactApplicationContext reactContext) {
    super(reactContext);
    reactContext.addActivityEventListener(this);
    reactContext.addLifecycleEventListener(this);
    nfcAdapter = NfcAdapter.getDefaultAdapter(reactContext);
  }

  @Override
  @NonNull
  public String getName() {
    return NAME;
  }

  private Activity getActivity() {
    return this.getCurrentActivity();
  }

  @Override
  public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
    this.sendLog("onActivityResult: requestCode: "
      + String.valueOf(requestCode) + " resultCode: "
      + String.valueOf(resultCode)
    );

    if (resultCode == Activity.RESULT_OK) {

      try {

      } catch (Exception e) {
        sendResponseEvent(Constant.EVENT.NFC_ERROR, ExceptionParser.parse(e));
      }
    } else {
      sendResponseEvent(Constant.EVENT.NFC_ERROR, ExceptionParser.parse(new Exception("onActivityResult error: requestCode: " + String.valueOf(requestCode) + ", resultCode" + String.valueOf(resultCode))));
    }
  }

  @Override
  public void onNewIntent(Intent intent) {
    sendLog("onNewIntent::::::::::");
    Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
    if (tag != null) {
      handleIntent(intent, false);
    }
  }

  @Override
  public void onHostResume() {
    sendLog("onHostResume::::::::::");
  }

  @Override
  public void onHostPause() {
    sendLog("onHostPause::::::::::");
    stopForegroundDispatch(getActivity(), this.nfcAdapter);
  }

  @Override
  public void onHostDestroy() {
    sendLog("onHostDestroy::::::::::");
    stopForegroundDispatch(getActivity(), this.nfcAdapter);
  }

  /**
   * @param activity The corresponding {@link Activity} requesting to stop the foreground dispatch.
   * @param adapter  The {@link NfcAdapter} used for the foreground dispatch.
   */
  public static void stopForegroundDispatch(final Activity activity, NfcAdapter adapter) {
    try {
      Log.d("NFC_INTERACT_LOG", "stopForegroundDispatch called");
      adapter.disableForegroundDispatch(activity);
    } catch (Exception e) {
      Log.d("NFC_INTERACT_LOG", "Error in stopping forground dispatch");
      Log.d("NFC_INTERACT_LOG", e.toString());
    }
  }

  /**
   * @param activity   The corresponding {@link Activity} requesting the foreground dispatch.
   * @param nfcAdapter The {@link NfcAdapter} used for the foreground dispatch.
   */
  public static void setupForegroundDispatch(final Activity activity, NfcAdapter nfcAdapter) {

    final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

    final PendingIntent pendingIntent = PendingIntent.getActivity(
      activity.getApplicationContext(), 0, intent, 0);

    IntentFilter[] filters = new IntentFilter[3];

    String[] filterNames = new String[]{
      NfcAdapter.ACTION_NDEF_DISCOVERED,
      NfcAdapter.ACTION_TAG_DISCOVERED,
      NfcAdapter.ACTION_TECH_DISCOVERED
    };

    int pos = 0;

    for (String filter : filterNames) {
      filters[pos] = new IntentFilter();
      filters[pos].addAction(filter);
      if (filter.equals(NfcAdapter.ACTION_NDEF_DISCOVERED)) {
        Log.d("NFC_INTERACT_LOG", filter + " gets a mime type added to it");
        try {
          filters[pos].addCategory(Intent.CATEGORY_DEFAULT);
          filters[pos].addDataType("*/*");
        } catch (IntentFilter.MalformedMimeTypeException e) {
          Log.d("NFC_INTERACT_LOG", "Check your mime type");
          throw new RuntimeException("Check your mime type.");
        }
      }
      pos++;
    }

    try {
      Log.d("NFC_INTERACT_LOG", "Starting Foreground Dispatch");
      nfcAdapter.enableForegroundDispatch(activity, pendingIntent, filters, techList);
    } catch (Exception e) {
      Log.d("NFC_INTERACT_LOG", "Failed enabling forground dispatch from permissions");
      Log.e("NFC_INTERACT_LOG", e.toString());
    }
  }

  @SuppressLint("LongLogTag")
  private void handleIntent(Intent intent, boolean startupIntent) {
    sendLog("handleIntent: " + intent.getAction());

    this.tag = (Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

    if (intent != null && intent.getAction() != null) {
      Log.d("NFC_INTERACT_LOG", "handling the intent");
      String serialNumber = getSerialNumber(this.tag);
      switch (intent.getAction()) {
        case NfcAdapter.ACTION_ADAPTER_STATE_CHANGED:
          Log.d("NFC_INTERACT_LOG", "ACTION_ADAPTER_STATE_CHANGED");
          break;

        case NfcAdapter.ACTION_NDEF_DISCOVERED:
          Log.d("NFC_INTERACT_LOG", "ACTION_NDEF_DISCOVERED");
          Parcelable[] rawMessages = intent.getParcelableArrayExtra(
            NfcAdapter.EXTRA_NDEF_MESSAGES);

          if (rawMessages != null) {
            NdefMessage[] messages = new NdefMessage[rawMessages.length];
            int pos = 0;
            for (Parcelable row : rawMessages) {
              Log.d("NFC_INTERACT_LOG:rawMsg", row.toString());
              messages[pos] = (NdefMessage) row;
              pos++;
            }
            processNdefMessages(serialNumber, messages, startupIntent);
          }
          break;

        case NfcAdapter.ACTION_TAG_DISCOVERED:
          Log.d("NFC_INTERACT_LOG", "ACTION_TAG_DISCOVERED");
          processTag(serialNumber, this.tag, startupIntent);
          break;
        case NfcAdapter.ACTION_TECH_DISCOVERED:
          Log.d("NFC_INTERACT_LOG", "ACTION_TECH_DISCOVERED");
          processTag(serialNumber, this.tag, startupIntent);
          break;
        default:
          Log.d("NFC_INTERACT_LOG:DEFAULT", intent.getAction());
      }
    }
  }

  private String getSerialNumber(Tag tag) {
    if (tag == null) {
      return "";
    } else {
      byte[] id = tag.getId();
      String serialNumber = Common.toHex(id);
      return serialNumber;
    }
  }

  private void processNdefMessages(String serialNumber, NdefMessage[] messages, boolean startupIntent) {
    System.out.printf("processNdefMessages::::::::::::::::::::::::::::::\n");
//        NdefProcessingTask task = new NdefProcessingTask(serialNumber, startupIntent);
//        task.execute(messages);
//        stopForegroundDispatch(getActivity(), adapter);
  }

  /**
   * This method is used to retrieve the NFC data was acquired before the React Native App was loaded.
   * It should be called only once, when the first listener is attached.
   * Subsequent calls will return null;
   *
   * @param callback callback passed by javascript to retrieve the nfc data
   */
  @ReactMethod
  public void getStartUpNfcData(Callback callback) {
    if (!startupNfcDataRetrieved) {
      callback.invoke(Common.cloneWritableMap(startupNfcData));
      startupNfcData = null;
      startupNfcDataRetrieved = true;
    } else {
      callback.invoke();
    }
  }

  @ReactMethod
  public void initialize() {
    if (nfcAdapter != null) {
      if (nfcAdapter.isEnabled()) {
        sendLog("EVENT_NFC_ENABLED THE THING IS ENABLED");
        sendResponseEvent(Constant.EVENT.NFC_ENABLED, null);
      } else {
        sendLog("EVENT_NFC_MISSING THE THING EXISTS BUT IS NOT ENABLED");
        sendResponseEvent(Constant.EVENT.NFC_MISSING, null);
      }
    } else {
      sendLog("EVENT_NFC_ENABLED THE THING IS SEEMS TO NOT HAVE THE THING");
      sendResponseEvent(Constant.EVENT.NFC_UNAVAILABLE, null);
    }
  }

  @ReactMethod
  public void doReadCardIsodep(String idNumber, String dateOfBirth, String dateOfExpiry) {
    this.sendLog(idNumber + " " + dateOfBirth + " " + dateOfExpiry);

    try {
      IsoDep isoDepTag = IsoDep.get(this.tag);
      BACKey bacKey = new BACKey(idNumber, dateOfBirth, dateOfExpiry);
      ReadCardChipIsoDepTask readCardChipIsoDepTask = new ReadCardChipIsoDepTask(isoDepTag, bacKey, getActivity());
      readCardChipIsoDepTask.execute();
      stopForegroundDispatch(getActivity(), this.nfcAdapter);
    } catch (Exception e) {
      sendResponseEvent(Constant.EVENT.NFC_ERROR, ExceptionParser.parse(e));
    }
  }

  private void sendLog(String mess) {
    WritableMap payloadLog = new WritableNativeMap();
    payloadLog.putString("message", mess);
    sendResponseEvent(Constant.EVENT.NFC_LOGGING, payloadLog);
  }

  private void onNFCTagDiscovered(@Nullable WritableMap payload) {
    payload.putString("origin", "android");

    getReactApplicationContext()
      .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
      .emit(Constant.EVENT.NFC_DISCOVERED, payload);
  }

  private void onReadCardSuccess(@Nullable WritableMap payload) {
    this.sendResponseEvent(Constant.EVENT.NFC_READ_CARD_SUCCESS, payload);
  }

  private void sendResponseEvent(String event, @Nullable WritableMap payload) {

    if (payload != null) {
      payload.putString("origin", "android");
    }

    getReactApplicationContext()
      .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
      .emit(event, payload);
  }

  private void processTag(String id, Tag tag, boolean startupIntent) {
    TagProcessingTask task = new TagProcessingTask(id, startupIntent);
    task.execute(tag);
    stopForegroundDispatch(getActivity(), this.nfcAdapter);
  }

  public class ReadCardChipIsoDepTask extends AsyncTask<Void, Void, Exception> {
    private IsoDep isoDep;
    private BACKeySpec bacKey;
    private Context context;

    private DG1File dg1File; //mrz
    private DG2File dg2File; //dữ liệu ảnh
    private DG14File dg14File;
    private SODFile sodFile;
    private String imageBase64;
    private Bitmap bitmap;
    private boolean chipAuthSucceeded = false;
    private boolean passiveAuthSuccess = false;
    private byte[] dg14Encoded = new byte[0];

    public ReadCardChipIsoDepTask(IsoDep isoDep, BACKeySpec bacKey, Context context) {
      this.isoDep = isoDep;
      this.bacKey = bacKey;
      this.context = context;
    }

    private void doChipAuth(PassportService service) {
      try {
        CardFileInputStream dg14In = service.getInputStream(PassportService.EF_DG14);
        dg14Encoded = IOUtils.toByteArray(dg14In);
        ByteArrayInputStream dg14InByte = new ByteArrayInputStream(dg14Encoded);
        dg14File = new DG14File(dg14InByte);

        Collection<SecurityInfo> dg14FileSecurityInfos = dg14File.getSecurityInfos();
        for (SecurityInfo securityInfo : dg14FileSecurityInfos) {
          if (securityInfo instanceof ChipAuthenticationPublicKeyInfo) {
            ChipAuthenticationPublicKeyInfo publicKeyInfo = (ChipAuthenticationPublicKeyInfo) securityInfo;
            BigInteger keyId = publicKeyInfo.getKeyId();
            PublicKey publicKey = publicKeyInfo.getSubjectPublicKey();
            String oid = publicKeyInfo.getObjectIdentifier();
            service.doEACCA(keyId, ChipAuthenticationPublicKeyInfo.ID_CA_ECDH_AES_CBC_CMAC_256, oid, publicKey);
            chipAuthSucceeded = true;
          }
        }
      } catch (Exception e) {
        Log.e("NFC_INTERACT_LOG", "ReadCardChipIsoDepTask -> doChipAuth: " + e.getMessage());
        sendResponseEvent(Constant.EVENT.NFC_ERROR, ExceptionParser.parse(e));
      }
    }

    @Override
    protected Exception doInBackground(Void... params) {
      try {
        this.isoDep.setTimeout(5000);
        CardService cardService = CardService.getInstance(this.isoDep);

        PassportService service = new PassportService(cardService, NORMAL_MAX_TRANCEIVE_LENGTH, DEFAULT_MAX_BLOCKSIZE, false, false);
        service.open();

        boolean paceSucceeded = false;

        try {
          CardAccessFile cardAccessFile = new CardAccessFile(service.getInputStream(PassportService.EF_CARD_ACCESS));
          Collection<SecurityInfo> securityInfoCollection = cardAccessFile.getSecurityInfos();

          for (SecurityInfo securityInfo : securityInfoCollection) {
            if (securityInfo instanceof PACEInfo) {
              PACEInfo paceInfo = (PACEInfo) securityInfo;
              service.doPACE(bacKey, paceInfo.getObjectIdentifier(), PACEInfo.toParameterSpec(paceInfo.getParameterId()));
              paceSucceeded = true;
            }
          }
        } catch (Exception e) {
          Log.e("NFC_INTERACT_LOG", "ReadCardChipIsoDepTask -> doInBackground: " + e.getMessage());
          sendResponseEvent(Constant.EVENT.NFC_ERROR, ExceptionParser.parse(e));
        }

        service.sendSelectApplet(paceSucceeded);

        if (!paceSucceeded) {
          try {
            service.getInputStream(PassportService.EF_COM).read();
          } catch (Exception e) {
            service.doBAC(bacKey);
          }
        }

        CardFileInputStream dg1In = service.getInputStream(PassportService.EF_DG1);
        dg1File = new DG1File(dg1In);

        CardFileInputStream dg2In = service.getInputStream(PassportService.EF_DG2);
        dg2File = new DG2File(dg2In);

        CardFileInputStream sodIn = service.getInputStream(PassportService.EF_SOD);
        sodFile = new SODFile(sodIn);

        // We perform Chip Authentication using Data Group 14
        doChipAuth(service);

        // Then Passive Authentication using SODFile
        doPassiveAuth();

        List<FaceImageInfo> allFaceImageInfos = new ArrayList<>();
        List<FaceInfo> faceInfos = dg2File.getFaceInfos();
        for (FaceInfo faceInfo : faceInfos) {
          allFaceImageInfos.addAll(faceInfo.getFaceImageInfos());
        }

        if (!allFaceImageInfos.isEmpty()) {
          FaceImageInfo faceImageInfo = allFaceImageInfos.iterator().next();

          int imageLength = faceImageInfo.getImageLength();
          DataInputStream dataInputStream = new DataInputStream(faceImageInfo.getImageInputStream());
          byte[] buffer = new byte[imageLength];
          dataInputStream.readFully(buffer, 0, imageLength);
          InputStream inputStream = new ByteArrayInputStream(buffer, 0, imageLength);

          bitmap = ImageUtil.decodeImage(this.context, faceImageInfo.getMimeType(), inputStream);
          imageBase64 = Base64.encodeToString(buffer, Base64.DEFAULT);
        }

      } catch (Exception e) {
        return e;
      }

      return null;
    }

    private void doPassiveAuth() {
      try {
        MessageDigest digest = MessageDigest.getInstance(sodFile.getDigestAlgorithm());

        Map<Integer, byte[]> dataHashes = sodFile.getDataGroupHashes();

        byte[] dg14Hash = new byte[0];
        if (chipAuthSucceeded) {
          dg14Hash = digest.digest(dg14Encoded);
        }
        byte[] dg1Hash = digest.digest(dg1File.getEncoded());
        byte[] dg2Hash = digest.digest(dg2File.getEncoded());

        if (Arrays.equals(dg1Hash, dataHashes.get(1)) && Arrays.equals(dg2Hash, dataHashes.get(2)) && (!chipAuthSucceeded || Arrays.equals(dg14Hash, dataHashes.get(14)))) {
          // We retrieve the CSCA from the german master list
          //HARD_CODE
          byte[] b = new byte[]{};
//                getAssets().open("masterList")
          ASN1InputStream asn1InputStream = new ASN1InputStream(b);
          ASN1Primitive p;
          KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
          keystore.load(null, null);
          CertificateFactory cf = CertificateFactory.getInstance("X.509");
          while ((p = asn1InputStream.readObject()) != null) {
            ASN1Sequence asn1 = ASN1Sequence.getInstance(p);
            if (asn1 == null || asn1.size() == 0) {
              throw new IllegalArgumentException("null or empty sequence passed.");
            }
            if (asn1.size() != 2) {
              throw new IllegalArgumentException("Incorrect sequence size: " + asn1.size());
            }
            ASN1Set certSet = ASN1Set.getInstance(asn1.getObjectAt(1));

            for (int i = 0; i < certSet.size(); i++) {
              Certificate certificate = Certificate.getInstance(certSet.getObjectAt(i));

              byte[] pemCertificate = certificate.getEncoded();

              java.security.cert.Certificate javaCertificate = cf.generateCertificate(new ByteArrayInputStream(pemCertificate));
              keystore.setCertificateEntry(String.valueOf(i), javaCertificate);
            }
          }
          List<X509Certificate> docSigningCertificates = sodFile.getDocSigningCertificates();
          for (X509Certificate docSigningCertificate : docSigningCertificates) {
            docSigningCertificate.checkValidity();
          }

          // We check if the certificate is signed by a trusted CSCA
          // TODO: verify if certificate is revoked
          CertPath cp = cf.generateCertPath(docSigningCertificates);
          PKIXParameters pkixParameters = new PKIXParameters(keystore);
          pkixParameters.setRevocationEnabled(false);
          CertPathValidator cpv = CertPathValidator.getInstance(CertPathValidator.getDefaultType());
          cpv.validate(cp, pkixParameters);

          String sodDigestEncryptionAlgorithm = sodFile.getDigestEncryptionAlgorithm();

          boolean isSSA = false;
          if (sodDigestEncryptionAlgorithm.equals("SSAwithRSA/PSS")) {
            sodDigestEncryptionAlgorithm = "SHA256withRSA/PSS";
            isSSA = true;
          }

          Signature sign = Signature.getInstance(sodDigestEncryptionAlgorithm);
          if (isSSA) {
            sign.setParameter(new PSSParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA256, 32, 1));
          }

          sign.initVerify(sodFile.getDocSigningCertificate());
          sign.update(sodFile.getEContent());
          passiveAuthSuccess = sign.verify(sodFile.getEncryptedDigest());
        }
      } catch (Exception e) {
        Log.e("NFC_INTERACT_LOG", "ReadCardChipIsoDepTask -> doPassiveAuth: " + e.getMessage());
        sendResponseEvent(Constant.EVENT.NFC_ERROR, ExceptionParser.parse(e));
      }
    }

    @Override
    protected void onPostExecute(Exception ex) {
      sendLog("onPostExecute::::::::::::");
      if (ex == null) {
        MRZInfo mrzInfo = dg1File.getMRZInfo();
        System.out.println("mrzInfo: " + StringUtils.objectToJson(mrzInfo));
        System.out.println("passiveAuthSuccess: " + String.valueOf(passiveAuthSuccess));
        System.out.println("chipAuthSucceeded: " + String.valueOf(chipAuthSucceeded));
        WritableMap data = RFIDParser.parse(mrzInfo, imageBase64, passiveAuthSuccess, chipAuthSucceeded);
        onReadCardSuccess(data);
      } else {
        Log.e("NFC_INTERACT_LOG", ex.getMessage());
        sendResponseEvent(Constant.EVENT.NFC_ERROR, ExceptionParser.parse(ex));
      }
    }
  }

  private class TagProcessingTask extends AsyncTask<Tag, Void, WritableMap> {

    private final String serialNumber;
    private final boolean startupIntent;

    TagProcessingTask(String serialNumber, boolean startupIntent) {
      this.serialNumber = serialNumber;
      this.startupIntent = startupIntent;
    }

    @Override
    protected WritableMap doInBackground(Tag... params) {
      Tag tag = params[0];
      WritableMap map = TagParser.parse(serialNumber, tag);
      Log.d("NFC_INTERACT_LOG", "" + tag.describeContents() + " <-> 0");
      int pos = 0;


      for (Tag param : params) {
        if (pos > 0) {
          map.merge(TagParser.parse(serialNumber + pos, param));
          Log.d("NFC_INTERACT_LOG", "" + param.describeContents() + " <-> " + pos);
        }
        pos++;
      }
      return map;
    }

    @Override
    protected void onPostExecute(WritableMap tagData) {
      if (startupIntent) {
        startupNfcData = tagData;
      }
      onNFCTagDiscovered(tagData);
    }

  }
}
